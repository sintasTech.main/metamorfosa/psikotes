<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->settingRole = config("settings.role");
    }

    public function redirectTo()
    {
        $authUserRole = auth()->user()->roles->first()->name;
        switch ($authUserRole) {
            case $this->settingRole["superadmin"]:
            case $this->settingRole["admin"]:
                return "/administrator";
                break;
            case "Participant":
                return "/participant";
                break;
            default:
                return "/";
                break;
        }
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {
            $user = $this->guard()->getLastAttempted();

            if ($user->is_active && $this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            } else {
                $this->incrementLoginAttempts($request);
                return redirect()
                    ->back()
                    ->withInput($request->only($this->username(), "remember"))
                    ->withErrors([$this->username() => "You must be active to login."]);
            }
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect()->route("login");
    }

    // use username
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => "required|string",
            "password" => "required|string",
        ]);
    }

    protected function credentials(Request $request)
    {
        return $request->only($this->username(), "password");
    }

    public function username()
    {
        return "username";
    }
}
