<?php

namespace App\Http\Controllers\Participant;

use App\Http\Controllers\Controller;

class DashboardsController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->authUserRole = auth()->user()->roles->first()->name;
            return $next($request);
        });
    }

    public function index()
    {
        $userTypes = auth()->user()->sessions->first()->types;
        // dd($userTypes->types());
        return view(
            "participant.dashboards.index",
            compact('userTypes')
        );
    }
}
