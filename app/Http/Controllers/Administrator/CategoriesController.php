<?php

namespace App\Http\Controllers\Administrator;

use App\Models\Category;
use App\Models\Type;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function create($typeId)
    {
        $type = Type::findOrFail($typeId);
        return view(
            "administrator.categories.create",
            compact("type")
        );
    }

    public function store(Request $request, $typeId)
    {
        $type = Type::findOrFail($typeId);
        $this->validate($request, [
            "name" => "required|string",
            "time" => "nullable|integer",
            "description" => "nullable|string",
            "rule" => "nullable|string",
            "example" => "nullable|string",
        ]);

        Category::updateOrCreate([
            "type_id" => $type->id,
            "name" => $request->name,
            "time" => $request->time ?? NULL,
            "description" => $request->description,
            "rule" => $request->rule,
            "example" => $request->example
        ]);

        return redirect()->route(
            "administrator.types.show",
            $type->id
        )->with("success", "Exam category type created successfully.");
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('administrator.categories.show', compact('category'));
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $types = Type::pluck("name", "id");

        return view(
            'administrator.categories.edit',
            compact('category', 'types')
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "type" => "required|exists:types,id",
            "name" => "required|string",
            "time" => "nullable|integer",
            "description" => "nullable|string",
            "rule" => "nullable|string",
            "example" => "nullable|string",
        ]);

        $category = Category::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "type" => $request->type,
                "name" =>  $request->name,
                "time" =>  $request->time ?? NULL,
                "description" =>  $request->description,
                "rule" =>  $request->rule,
                "example" =>  $request->example
            ]
        );

        return redirect()->route(
            "administrator.categories.edit",
            $category->id
        )->with("success", "Exam category type updated successfully.");
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $typeId = $category->type->id;
        $category->delete();

        return redirect()->route(
            'administrator.types.show',
            $typeId
        )->with("success", "Exam category type deleted successfully.");
    }
}
