<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Config;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use App\User;
use App\Models\Session;
use App\Models\Type;

class DashboardsController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->authUserRole = auth()->user()->roles->first()->name;
            return $next($request);
        });
    }

    public function index()
    {
        $authUserRole = $this->authUserRole;
        if ($authUserRole === $this->settingRole["superadmin"]) {
            $permissions = Permission::all();
            $roles = Role::all();
            $users = User::all();

            $compact = compact("users", "permissions", "roles");
        } else {
            $users = User::whereHas(
                "roles",
                function ($role) {
                    $role->where("name", "!=", $this->settingRole["superadmin"]);
                }
            )->get();

            $compact = compact("users");
        }

        $sessionCount = Session::count();
        $participantCount = User::whereHas("roles", function ($roles) {
                                    $roles->where("name", "participant");
                                }
                            )->count();
        $typeCount = Type::count();

        return view(
            "administrator.dashboards.index",
            compact('sessionCount', 'participantCount', 'typeCount')
        );
    }
}
