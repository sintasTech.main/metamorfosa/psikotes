<?php

namespace App\Http\Controllers\Administrator;

use App\Models\Type;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TypesController extends Controller
{
    public function index()
    {
        $types = Type::paginate(10);
        return view('administrator.types.index', compact('types'));
    }

    public function create()
    {
        return view('administrator.types.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|string",
            "description" => "nullable|string",
        ]);

        $payload = $request->all();
        Type::create($payload);

        return redirect()->route(
            "administrator.types.index"
        )->with("success", "Type Test created successfully.");
    }

    public function show($id)
    {
        $type = Type::findOrFail($id);
        return view('administrator.types.show', compact('type'));
    }

    public function edit($id)
    {
        $type = Type::findOrFail($id);
        return view('administrator.types.edit', compact('type'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required|string",
            "description" => "nullable|string",
        ]);
        $payload = $request->all();
        $type = Type::findOrFail($id);
        $type->update($payload);

        return redirect()->route(
            "administrator.types.index"
        )->with("success", "Type Test updated successfully.");
    }

    public function destroy($id)
    {
        $type = Type::findOrFail($id);
        $type->delete();

        return redirect()->route(
            'administrator.types.index'
        )->with("success", "Type Test deleted successfully.");
    }
}
