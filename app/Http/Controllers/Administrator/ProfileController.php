<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Rules\MatchOldPassword;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Models\Person;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            $this->authUserRole = $this->user->roles->first()->name;
            $this->routeUserPrefix = strtolower(in_array($this->authUserRole, $this->settingRole) ? "Administrator" : $this->authUserRole);

            return $next($request);
        });
    }

    public function index()
    {
        $id = $this->user->id;
        $user = User::findOrFail($id);
        $roles = Role::orderBy("name")->pluck("name", "id")->all();
        $userRole = $user->roles->pluck("id", "name")->first();

        if ($this->authUserRole !== $this->settingRole["superadmin"]) {
            $superadminRoleId = Role::where("name", $this->settingRole["superadmin"])->pluck("id")->first();
            unset($roles[$superadminRoleId]);
        }

        return view(
            "administrator.profile.index",
            compact("user", "roles", "userRole")
        );
    }

    public function update(Request $request)
    {
        $id = $this->user->id;
        $this->validate($request, [
            "name" => "required|string|max:255",
            // "email" => "required|email|max:255|unique:users,email," . $id
            "username" => "required|string|max:255|unique:users,username," . $id
        ]);
        $input = $request->all();

        $user = User::findOrFail($id);
        $user->update($input);
        Person::updateOrCreate(
            [
                "user_id" => $user->id
            ],
            [
                "full_name" => $request->input("name")
            ]
        );

        if ($request->input("role")) {
            DB::table("model_has_roles")->where("model_id", $id)->delete();
            $user->assignRole([$request->input("role")]);
        }

        return redirect()->route(
            $this->routeUserPrefix . ".profile.index"
        )->with("success", "Success! Your profile has been updated!");
    }

    public function destroy()
    {
        Auth::logout();
        $id = $this->user->id;
        User::findOrFail($id)->delete();

        return redirect()->route(
            $this->routeUserPrefix . ".profile.index"
        );
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            "current_password" => ["required", new MatchOldPassword],
            "new_password" => "required|same:new_confirm_password",
        ]);

        $id = $this->user->id;
        User::findOrFail($id)->update(["password" => Hash::make($request->new_password)]);

        return redirect()->route(
            $this->routeUserPrefix . ".profile.index"
        )->with("success", "Success! Your password has been updated!");
    }

    public function updateAvatar(Request $request)
    {
        $request->validate([
            "avatar"        => "required|mimes:jpg,jpeg,png"
        ]);

        $fileName = date("Y_m_d_His") . "_" . uniqid() . "." . $request->file("avatar")->extension();
        $filePath = $request->file("avatar")->storeAs(
            "storage/images/avatars",
            $fileName,
            "public"
        );

        $id = $this->user->id;
        $person = Person::whereUserId($id)->first();
        if ($filePath) {
            if (!preg_match("/^(\/)?seeds/i", $person->avatar)) {
                Storage::disk("public")->delete($person->avatar);
            }

            Person::updateOrCreate(
                [
                    "user_id"     => $person->user_id
                ],
                [
                    "avatar"    => $filePath
                ]
            );
        }

        return redirect()->route(
            $this->routeUserPrefix . ".profile.index"
        )->with("success", "Success! Your avatar has been updated!");
    }
}
