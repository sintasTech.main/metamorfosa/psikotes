<?php

namespace App\Http\Controllers\Administrator;

use App\Models\Session;
use App\Http\Controllers\Controller;
use App\Models\Type;
use Illuminate\Http\Request;

class SessionsController extends Controller
{
    public function index(Request $request)
    {
        $sessions = Session::paginate(10);
        return view('administrator.sessions.index', compact('sessions'));
    }

    public function create()
    {
        $types = Type::pluck("name", "id");
        return view(
            'administrator.sessions.create',
            compact("types")
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|string",
            "description" => "nullable|string",
            "type" => "required",
            "type.*" => "required|exists:types,id",
        ]);

        $session = Session::updateOrCreate([
            "name" => $request->name,
            "description" => $request->description
        ]);

        $session->types()->sync($request->type);

        return redirect()->route(
            "administrator.sessions.index"
        )->with("success", "Session created successfully.");
    }

    public function show($id)
    {
        $session = Session::findOrFail($id);
        return view(
            'administrator.sessions.show',
            compact('session')
        );
    }

    public function edit($id)
    {
        $session = Session::findOrFail($id);
        $types = Type::pluck("name", "id");

        return view(
            'administrator.sessions.edit',
            compact("session", "types")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required|string",
            "description" => "nullable|string",
            "type" => "required",
            "type.*" => "required|exists:types,id",
        ]);

        $session = Session::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "name" => $request->name,
                "description" => $request->description
            ]
        );

        $session->types()->sync($request->type);

        return redirect()->back()->with("success", "Session updated successfully.");
    }

    public function destroy($id)
    {
        $session = Session::findOrFail($id);
        $session->delete();

        return redirect()->route(
            'administrator.sessions.index'
        )->with("success", "Session deleted successfully.");
    }
}
