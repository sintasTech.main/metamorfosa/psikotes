<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\Question;

class QuestionsController extends Controller
{
    public function create($categoryId)
    {
        $category = Category::findOrFail($categoryId);
        return view(
            "administrator.questions.create",
            compact("category")
        );
    }

    public function store(Request $request, $categoryId)
    {
        $category = Category::findOrFail($categoryId);
        $this->validate($request, [
            "description" => "required",
            "answer_type" => "required|in:vertical,horizontal"
        ]);

        Question::create([
            "category_id" => $category->id,
            "description" => $request->input("description"),
            "answer_type" => strtolower($request->answer_type)
        ]);

        return redirect()->route(
            "administrator.categories.show",
            $category->id
        )->with("success", "Questions created successfully.");
    }

    public function edit($id)
    {
        $question = Question::findOrFail($id);
        return view(
            "administrator.questions.edit",
            compact("question")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        $question = Question::findOrFail($id);
        $question->name = $request->input("name");
        $question->save();

        return redirect()->route(
            "administrator.questions.edit",
            $question->id
        )->with("success", "Question updated successfully.");
    }

    public function destroy($id)
    {
        $question = Question::findOrFail($id);
        $categoryId = $question->category->id;
        $question->delete();

        return redirect()->route(
            "administrator.categories.show",
            $categoryId
        )->with("success", "Question deleted successfully.");
    }

    public function uploadImage(Request $request)
    {
        if ($request->hasFile("file")) {
            $fileName = date("Y_m_d_His") . "_" . uniqid() . "." . $request->file("file")->extension();

            $filePath = $request->file("file")->storeAs(
                "storage/images/questions",
                $fileName,
                "public"
            );

            return json_encode(["location" => url($filePath)]);
        }
    }
}
