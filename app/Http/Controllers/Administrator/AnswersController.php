<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Answer;

class AnswersController extends Controller
{
    public function index(Request $request)
    {
        return view(
            "administrator.answers.index"
        )->with("i", ($request->input("page", 1) - 1) * 5);
    }

    public function create()
    {
        return view(
            "administrator.answers.create"
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:roles,name"
        ]);
        Answer::create([
            "name" => $request->input("name"),
            "guard_name" => "web"
        ]);

        return redirect()->route(
            "administrator.answers.index"
        )->with("success", "Answers created successfully.");
    }

    public function edit($id)
    {
        $answer = Answer::findOrFail($id);
        return view(
            "administrator.answers.edit",
            compact("answer")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        $answer = Answer::findOrFail($id);
        $answer->name = $request->input("name");
        $answer->save();

        return redirect()->route(
            "administrator.answers.edit",
            $Answer->id
        )->with("success", "Answer updated successfully.");
    }

    public function destroy($id)
    {
        Answer::findOrFail($id)->delete();
        return redirect()->route(
            "administrator.answers.index"
        )->with("success", "Answer deleted successfully.");
    }
}
