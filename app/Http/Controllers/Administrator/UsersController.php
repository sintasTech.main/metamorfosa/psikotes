<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Rules\MatchOldPassword;

use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use App\User;
use App\Models\Person;
use App\Models\Session;
use App\Models\SessionUser;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");

        $this->middleware(function ($request, $next) {
            $this->authUserId = auth()->user()->id;
            $this->authUserRole = auth()->user()->roles->first()->name;
            return $next($request);
        });
    }

    public function index(Request $request)
    {
        if ($this->authUserRole === $this->settingRole["superadmin"]) {
            $users = User::whereHas(
                "person",
                function ($people) {
                    $people->orderBy("full_name");
                }
            )->get();
        } else {
            $users = User::whereHas(
                "roles",
                function ($roles) {
                    $roles->where("name", "!=", $this->settingRole["superadmin"]);
                }
            )->whereHas(
                "person",
                function ($people) {
                    $people->orderBy("full_name");
                }
            )->get();
        }

        return view(
            "administrator.users.index",
            compact("users")
        )->with('i', ($request->input("page", 1) - 1) * 10);
    }

    public function destroy($id)
    {
        if ($this->authUserId === (int) $id) {
            Auth::logout();
        }
        User::findOrFail($id)->delete();

        return redirect()->back()->with("success", "User deleted successfully.");
    }

    public function adminIndex(Request $request)
    {
        $users = User::whereHas(
            "roles",
            function ($roles) {
                $roles->where("name", $this->settingRole["admin"]);
            }
        )->whereHas(
            "person",
            function ($people) {
                $people->orderBy("full_name");
            }
        )->get();

        return view(
            "administrator.users.admin.index",
            compact("users")
        )->with('i', ($request->input("page", 1) - 1) * 5);
    }

    public function adminCreate()
    {
        return view(
            "administrator.users.admin.create"
        );
    }

    public function adminStore(Request $request)
    {
        $this->validate($request, [
            "name" => "required|string|max:255",
            "email" => "required|email|max:255|unique:users",
            "username" => "required|string|max:255|unique:users",
            "password" => "required|string|min:6|confirmed"
        ]);
        $input = $request->all();

        $input["password"] = Hash::make($input["password"]);
        $user = User::create($input);
        Person::updateOrCreate(
            [
                "user_id" => $user->id,
            ],
            [
                "full_name" => $request->input("name")
            ]
        );
        $role = Role::whereName("Admin")->first();
        $user->assignRole([$role->id]);

        return redirect()->route(
            "administrator.users.admin.index"
        )->with("success", "User created successfully.");
    }

    public function adminEdit($id)
    {
        $user = User::findOrFail($id);

        return view(
            "administrator.users.admin.edit",
            compact("user")
        );
    }

    public function adminUpdate(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required|string|max:255",
            "email" => "required|email|max:255|unique:users,email," . $id,
            "username" => "required|string|max:255|unique:users,username," . $id
        ]);
        $input = $request->all();

        $user = User::findOrFail($id);
        $user->update($input);
        Person::updateOrCreate(
            [
                "user_id" => $user->id
            ],
            [
                "full_name" => $request->input("name")
            ]
        );

        return redirect()->back()->with("success", "User updated successfully.");
    }

    public function participantIndex(Request $request)
    {
        if ($request->session) {
            $session = Session::find($request->session);
            $users = $session->users;
        } else {
            $users = User::whereHas(
                "roles",
                function ($roles) {
                    $roles->where("name", "participant");
                }
            )->whereHas(
                "person",
                function ($people) {
                    $people->orderBy("full_name");
                }
            )->get();
        }

        return view(
            "administrator.users.participant.index",
            compact("users")
        )->with('i', ($request->input("page", 1) - 1) * 10);
    }

    public function participantCreate()
    {
        $sessions = Session::pluck('name', 'id');
        return view(
            "administrator.users.participant.create",
            compact("sessions")
        );
    }

    public function participantStore(Request $request)
    {
        $this->validate($request, [
            "session" => "required|exists:sessions,id",
            "participant_count" => "required|numeric|min:0|not_in:0"
        ]);

        $session = Session::find($request->session);
        for ($i = 0; $i < $request->participant_count; $i++) {
            do {
                $pool = "0123456789abcdefghijklmnopqrstuvwxyz";
                $length = mt_rand(7, 10);
                $username = strtolower(substr(str_shuffle(str_repeat($pool, 5)), 0, $length));

                $user = User::create([
                    'username' => $username,
                    'password' => Hash::make($username)
                ]);
            } while (!$user);

            Person::updateOrCreate(
                [
                    "user_id" => $user->id,
                ],
                [
                    "full_name" => $user->username
                ]
            );
            $role = Role::whereName("Participant")->first();
            $user->assignRole([$role->id]);

            $user = SessionUser::create([
                'session_id' => $session->id,
                'user_id' => $user->id
            ]);
        }

        return redirect()->route(
            "administrator.users.participant.index"
        )->with("success", "User created successfully.");
    }

    public function participantEdit($id)
    {
        $user = User::findOrFail($id);
        $sessions = Session::pluck('name', 'id');

        return view(
            "administrator.users.participant.edit",
            compact("user", "sessions")
        );
    }

    public function participantUpdate(Request $request, $id)
    {
        $this->validate($request, [
            "session" => "required|exists:sessions,id",
            "username" => "required|string|max:255|unique:users,username," . $id
        ]);

        $user = User::updateOrCreate(
            [
                "id" => $id
            ],
            [
                "username" => strtolower($request->username)
            ]
        );

        Person::updateOrCreate(
            [
                "user_id" => $user->id
            ],
            [
                "full_name" => $user->username
            ]
        );

        SessionUser::updateOrCreate(
            [
                "user_id" => $user->id
            ],
            [
                "session_id" => $request->session
            ]
        );

        return redirect()->back()->with("success", "User updated successfully.");
    }

    public function updatePassword(Request $request, $id)
    {
        $request->validate([
            "current_password" => ["required", new MatchOldPassword],
            "new_password" => "required|same:new_confirm_password",
        ]);
        User::findOrFail($id)->update(["password" => Hash::make($request->new_password)]);

        return redirect()->back()->with("success", "Password successfully updated.");
    }

    public function updateStatus(Request $request, $id)
    {
        User::updateOrCreate(
            [
                "id"        => $id
            ],
            [
                "is_active"    => $request->get("status")
            ]
        );

        if ($this->authUserId === (int) $id && $request->get("status") === "0") {
            Auth::logout();
        }

        return response()->json([
            "status" => true,
            "message" => "Status change successfully."
        ]);
    }

    public function updateAvatar(Request $request, $id)
    {
        $request->validate([
            "avatar"        => "required|mimes:jpg,jpeg,png"
        ]);

        $fileName = date("Y_m_d_His") . "_" . uniqid() . "." . $request->file("avatar")->extension();
        $filePath = $request->file("avatar")->storeAs(
            "storage/images/avatars",
            $fileName,
            "public"
        );

        $person = Person::whereUserId($id)->first();
        if ($filePath) {
            if (!preg_match("/^(\/)?seeds/i", $person->avatar)) {
                Storage::disk("public")->delete($person->avatar);
            }

            Person::updateOrCreate(
                [
                    "user_id"     => $person->user_id
                ],
                [
                    "avatar"    => $filePath
                ]
            );
        }

        return redirect()->back()->with("success", "Avatar successfully updated.");
    }
}
