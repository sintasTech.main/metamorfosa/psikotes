<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Permission;

class PermissionsController extends Controller
{
    function __construct()
    {
        $this->middleware("permission:permission-list|permission-create|permission-edit|permission-delete", ["only" => ["index", "store"]]);
        $this->middleware("permission:permission-create", ["only" => ["create", "store"]]);
        $this->middleware("permission:permission-edit", ["only" => ["edit", "update"]]);
        $this->middleware("permission:permission-delete", ["only" => ["destroy"]]);
    }

    public function index(Request $request)
    {
        $permissions = Permission::orderBy("name")->paginate(10);
        return view(
            "administrator.permissions.index",
            compact("permissions")
        )->with("i", ($request->input("page", 1) - 1) * 5);
    }

    public function create()
    {
        return view(
            "administrator.permissions.create"
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:roles,name"
        ]);
        Permission::create([
            "name" => $request->input("name"),
            "guard_name" => "web"
        ]);

        return redirect()->route(
            "administrator.permissions.index"
        )->with("success", "Permission created successfully.");
    }

    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        return view(
            "administrator.permissions.edit",
            compact("permission")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required"
        ]);

        $permission = Permission::findOrFail($id);
        $permission->name = $request->input("name");
        $permission->save();

        return redirect()->route(
            "administrator.permissions.edit",
            $permission->id
        )->with("success", "Permission updated successfully.");
    }

    public function destroy($id)
    {
        Permission::findOrFail($id)->delete();
        return redirect()->route(
            "administrator.permissions.index"
        )->with("success", "Permission deleted successfully.");
    }
}
