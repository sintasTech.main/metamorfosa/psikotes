<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Illuminate\Support\Facades\DB;

class RolesController extends Controller
{
    function __construct()
    {
        $this->middleware("permission:role-list|role-create|role-edit|role-delete", ["only" => ["index", "store"]]);
        $this->middleware("permission:role-create", ["only" => ["create", "store"]]);
        $this->middleware("permission:role-edit", ["only" => ["edit", "update"]]);
        $this->middleware("permission:role-delete", ["only" => ["destroy"]]);
    }

    public function index(Request $request)
    {
        $roles = Role::orderBy("name")->paginate(10);
        return view(
            "administrator.roles.index",
            compact("roles")
        )->with("i", ($request->input("page", 1) - 1) * 5);
    }

    public function create()
    {
        $permissions = Permission::all()->sortBy("name");
        return view(
            "administrator.roles.create",
            compact("permissions")
        );
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|unique:roles,name",
        ]);
        $role = Role::create(["name" => $request->input("name")]);
        if ($request->input("permission")) {
            $role->syncPermissions($request->input("permission"));
        }

        return redirect()->route(
            "administrator.roles.index"
        )->with("success", "Role created successfully.");
    }

    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all()->sortBy("name");
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id", $id)
            ->pluck("role_has_permissions.permission_id", "role_has_permissions.permission_id")
            ->all();

        return view(
            "administrator.roles.edit",
            compact("role", "permissions", "rolePermissions")
        );
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            "name" => "required",
        ]);

        $role = Role::findOrFail($id);
        $role->name = $request->input("name");
        $role->save();
        if ($request->input("permission")) {
            $role->syncPermissions($request->input("permission"));
        } else {
            DB::table("role_has_permissions")->where("role_id", $id)->delete();
        }

        return redirect()->route(
            "administrator.roles.edit",
            $role->id
        )->with("success", "Role updated successfully.");
    }

    public function destroy($id)
    {
        Role::findOrFail($id)->delete();
        return redirect()->route(
            "administrator.roles.index"
        )->with("success", "Role deleted successfully.");
    }
}
