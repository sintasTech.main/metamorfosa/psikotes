<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

use App\Helpers\ResponseHelper;

class JwtMiddleware extends BaseMiddleware
{
    public function __construct()
    {
        $this->responHelper = new ResponseHelper;
    }

    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json([
                    "meta" => [
                        "code" => 401,
                        "message" => "Token is Invalid.",
                        "error" => true
                    ]
                ]);
            } else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json([
                    "meta" => [
                        "code" => 401,
                        "message" => "Token is Expired.",
                        "error" => true
                    ]
                ]);
            } else {
                return response()->json([
                    "meta" => [
                        "code" => 401,
                        "message" => "Authorization Token not found.",
                        "error" => true
                    ]
                ]);
            }
        }

        return $next($request);
    }
}
