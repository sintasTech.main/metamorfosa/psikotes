<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;

use Closure;

use Illuminate\Support\Facades\Auth;

class CheckUserStatus extends Middleware
{
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user) {
            if (!$user = auth()->user()->is_active) {
                Auth::logout();
                return redirect()->route("login");
            }
        }
        return $next($request);
    }
}
