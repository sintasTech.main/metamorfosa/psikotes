<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    public function __construct()
    {
        $this->settingRole = config("settings.role");
    }

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $authUserRole = auth()->user()->roles->first()->name;
            switch ($authUserRole) {
                case $this->settingRole["superadmin"]:
                case $this->settingRole["admin"]:
                    return redirect()->route("administrator.dashboards");
                    break;
                default:
                    return redirect()->route(strtolower($authUserRole) . ".dashboards");
                    break;
            }
        }

        return $next($request);
    }
}
