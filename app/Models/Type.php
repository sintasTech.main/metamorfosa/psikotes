<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $fillable = [
        "name",
        "description",
        "time"
    ];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function session()
    {
        return $this->belongsToMany(Type::class, "session_types", "type_id", "session_id");
    }
}
