<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        "name",
        "description"
    ];

    public function users()
    {
        return $this->belongsToMany(\App\User::class, "session_users", "session_id", "user_id");
    }

    public function types()
    {
        return $this->belongsToMany(Type::class, "session_types", "session_id", "type_id");
    }
}
