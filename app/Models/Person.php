<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        "user_id",
        "full_name",
        "avatar"
    ];

    public function users()
    {
        return $this->hasMany(User::class, "id");
    }
}
