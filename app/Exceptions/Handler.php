<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    protected $dontReport = [
        //
    ];

    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function render($request, Throwable $exception)
    {
        $settingRole = config("settings.role");
        if ($request->wantsJson() || preg_match("/^api/i", $request->path())) {
            return $this->handleApiException($request, $exception);
        } else {
            if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
                // $authUserRole = auth()->user()->roles->first()->name;
                // $routeUserPrefix = strtolower(in_array($authUserRole, $settingRole) ? "Administrator" : $authUserRole);
                // return response()->view($routeUserPrefix . ".errors.500");
                return response()->view("administrator.errors.500");
            }

            return parent::render($request, $exception);
        }
    }

    private function handleApiException($request, Exception $exception)
    {
        $exception = $this->prepareException($exception);

        if ($exception instanceof \Illuminate\Http\Exception\HttpResponseException) {
            $exception = $exception->getResponse();
        }

        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            $exception = $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            $exception = $this->convertValidationExceptionToResponse($exception, $request);
        }

        return $this->customApiResponse($exception);
    }

    private function customApiResponse($exception)
    {
        if (method_exists($exception, "getStatusCode")) {
            $statusCode = $exception->getStatusCode();
        } else {
            $statusCode = 500;
        }

        switch ($statusCode) {
            case 401:
                $message = "Unauthorized.";
                break;
            case 403:
                $message = "Forbidden.";
                break;
            case 404:
                $message = "Not Found.";
                break;
            case 405:
                $message = "Method Not Allowed.";
                break;
            case 422:
                $message = $exception->original["message"];
                $response["errors"] = $exception->original["errors"];
                break;
            default:
                $message = $statusCode == 500 ? "Whoops, looks like something went wrong." : $exception->getMessage();
                break;
        }

        return response()->json([
            "meta" => [
                "code" => $statusCode,
                "message" => $message,
                "error" => true
            ]
        ]);
    }
}
