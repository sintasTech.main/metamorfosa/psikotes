<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {
            $settings = config("settings");

            if (auth()->check()) {
                $authUserRole = auth()->user()->roles->first()->name;
                $routeUserPrefix = strtolower(in_array($authUserRole, $settings["role"]) ? "Administrator" : $authUserRole);
            } else {
                $authUserRole = "";
                $routeUserPrefix = "";
            }

            $view->with([
                "settingRole" => $settings["role"],
                "authUserRole" => $authUserRole,
                "routeUserPrefix" => $routeUserPrefix,
            ]);
        });
    }
}
