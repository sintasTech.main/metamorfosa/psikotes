<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    @yield('css')
</head>

<body>
    <div class="flex-center position-ref full-height">
        @include('home.layouts.navbar')

        <div class="content">
            @yield('contents')
        </div>
    </div>

    @yield('js')
</body>

</html>