@if (Route::has('login'))
<div class="top-right links">
    @auth
    <a href="{{ route('login') }}">Manage</a>
    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout').submit();">
        {{ __('Logout') }}
    </a>

    <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    @else
    <a href="{{ route('login') }}">Login</a>

    @if (Route::has('register'))
    <a href="{{ route('register') }}">Register</a>
    @endif
    @endauth
</div>
@endif