@extends('auth.layouts.template')

@section('content')
<!-- title-->
<h4 class="mt-0">Sign In</h4>
<p class="text-muted mb-4">Enter your username and password to access account.</p>

<!-- form -->
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="form-group">
        <label for="username">Username</label>
        <input class="form-control @error('username') is-invalid @enderror" type="text" id="username" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus placeholder="Enter your username">

        @error('username')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}" class="text-muted float-right"><small>Forgot your password?</small></a>
        @endif
        
        <label for="password">Password</label>
        <div class="input-group input-group-merge">
            <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter your password">
            <div class="input-group-append" data-password="false">
              <div class="input-group-text">
                <span class="password-eye"></span>
              </div>
            </div>

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
    </div>
    
    <div class="form-group mb-3">
        <div class="custom-control custom-checkbox">
            <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="custom-control-label" for="checkbox-signin">Remember me</label>
        </div>
    </div>
    <div class="form-group mb-0 text-center">
        <button class="btn btn-primary btn-block" type="submit">Log In</button>
    </div>
    <!-- social-->
    <div class="text-center mt-4">
        <!-- <p class="text-muted font-16">Sign in with</p> -->
        <ul class="social-list list-inline mt-3">
            <li class="list-inline-item">
                <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="javascript: void(0);" class="social-list-item border-info text-info"><i class="mdi mdi-twitter"></i></a>
            </li>
            <li class="list-inline-item">
                <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i class="mdi mdi-github"></i></a>
            </li>
        </ul>
    </div>
</form>
<!-- end form-->
@endsection