<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('themes/ubold/images/metamorfosa.ico') }}">

    <!-- App css -->
    <link href="{{ asset('themes/ubold/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{ asset('themes/ubold/css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <!-- icons -->
    <link href="{{ asset('themes/ubold/css/icons.min.css') }}" rel="stylesheet" type="text/css" />


    @yield('css')
</head>

<body class="auth-fluid-pages pb-0">
    <div class="auth-fluid">
        <!--Auth fluid left content -->
        <div class="auth-fluid-form-box">
            <div class="align-items-center d-flex h-100">
                <div class="card-body">
                    
                    <!-- Logo -->
                    <div class="auth-brand text-center text-lg-left">
                        <div class="auth-logo">
                            <a href="/" class="logo logo-dark text-center">
                                <span class="logo-lg">
                                    <img src="{{ asset('themes/ubold/images/metamorfosa.png') }}" alt="" height="82">
                                </span>
                            </a>
                            
                            <a href="/" class="logo logo-light text-center">
                                <span class="logo-lg">
                                    <img src="{{ asset('themes/ubold/images/metamorfosa.png') }}" alt="" height="82">
                                </span>
                            </a>
                        </div>
                    </div>
                    
                    @yield('content')

                </div> <!-- end .card-body -->
            </div> <!-- end .align-items-center.d-flex.h-100-->
        </div>
        <!-- end auth-fluid-form-box-->

        <!-- Auth fluid right content -->
        <div class="auth-fluid-right text-center">
            <div class="auth-user-testimonial">
                <h2 class="mb-3 text-white">I love the color!</h2>
                <p class="lead"><i class="mdi mdi-format-quote-open"></i> I've been using your theme from the previous developer for our web app, once I knew new version is out, I immediately bought with no hesitation. Great themes, good documentation with lots of customization available and sample app that really fit our need. <i class="mdi mdi-format-quote-close"></i>
                </p>
                <h5 class="text-white">
                    - Fadlisaad (Ubold Admin User)
                </h5>
            </div> <!-- end auth-user-testimonial-->
        </div>
        <!-- end Auth fluid right content -->
    </div>

    <!-- Vendor js -->
    <script src="{{ asset('themes/ubold/js/vendor.min.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('themes/ubold/js/app.min.js') }}"></script>

    @yield('js')
</body>

</html>