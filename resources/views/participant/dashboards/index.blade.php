@extends('participant.layouts.template')

@section('title', 'Psikotes Online')

@section('content')
<div class="content">
    <!-- Start Content-->

    @include('participant.layouts.welcome')
    
    <div class="container-fluid">
        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <h4 class="page-title test">Silahkan Pilih Kategori Test</h4>
                </div>
            </div>
        </div>     
        <!-- end page title  -->
        <div class="row">
            @foreach($userTypes as $type)
            <div class="col-md-6 col-xl-4">
                <div class="course">
                    <div class="course-preview">
                    </div>
                    <div class="course-info">
                        <h6>Course</h6>
                        <h3>{{ $type->name }}</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet repellendus illo deleniti quo molestiae at, blanditiis incidunt esse sed omnis cupiditate architecto. Itaque animi libero asperiores exercitationem quisquam tenetur beatae!</p>
                        <div class="d-flex flex-column justify-content-between">
                            <p class=""><i class="mdi mdi-book-multiple"></i> 120 Soal</p>
                            <p class=""><i class="mdi mdi-clock"></i> 90 Menit</p>
                        </div>
                    </div>
                    <div class="course-action d-flex justify-content-end">
                        <a href="{{ route('participant.types.show', $type->id) }}" class="btn">Mulai Ujian</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        
    </div> <!-- container -->

</div> <!-- content -->
@endsection

@section('javascript')
<script>
    // $('#test').on('click', function(){
    //     alert('test')
    // })
</script>
@endsection