<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js h-100">

    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <?php $route = explode(".", Illuminate\Support\Facades\Route::currentRouteName()); ?>
        <title>@yield('title') &mdash; {{ config("app.name", "Laravel") }}</title>
        <meta name="description" content="Made with love by sintasTech">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('themes/ubold/images/metamorfosa.png') }}{{ asset('themes/ubold/images/favicon.ico') }}">

        <!-- Plugins css -->
        <link href="{{ asset('themes/ubold/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('themes/ubold/libs/selectize/css/selectize.bootstrap3.css') }}" rel="stylesheet" type="text/css" />
        
        <!-- App css -->
        <link href="{{ asset('themes/ubold/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
        <link href="{{ asset('themes/ubold/css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

        <link href="{{ asset('themes/ubold/css/bootstrap-dark.min.css') }}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
        <link href="{{ asset('themes/ubold/css/app-dark.min.css') }}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />
        <link rel="stylesheet" href="{{ asset('themes/ubold/css/custom.css') }}">

        <!-- icons -->
        <link href="{{ asset('themes/ubold/css/icons.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- additional css -->
        @yield('css')
    </head>

    <body data-layout-mode="horizontal" data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>

        <!-- Begin page -->
        <div id="wrapper">
            <!-- Topbar Start -->
            @include('participant.layouts.navbar')
            <!-- end Topbar -->
            @if( $route[1] === 'dashboards')
            <div class="topnav shadow-lg">
                <div class="container-fluid">
                    <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                        <div class="collapse navbar-collapse active" id="topnav-menu-content">
                            <ul class="navbar-nav active">
                                <li class="nav-item">
                                    <p class="nav-link m-0">
                                        <strong>Hallo {{ auth()->user()->person->full_name }},</strong> Selamat datang di Sistem Ujian Psikotes Online <strong>Metamorfosa.</strong>
                                    </p>
                                </li>
                            </ul> <!-- end navbar-->
                        </div> <!-- end .collapsed-->
                    </nav>
                </div> <!-- end container-fluid -->
            </div>
            @endif

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">

                @yield('content')
            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->

        </div>
        <!-- END wrapper -->

        <!-- Right Sidebar -->
        @include('participant.layouts.sidebar')
        <!-- /Right-bar -->

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{ asset('themes/ubold/js/vendor.min.js') }}"></script>

        <!-- Plugins js-->
        <script src="{{ asset('themes/ubold/libs/flatpickr/flatpickr.min.js') }}"></script>
        <script src="{{ asset('themes/ubold/libs/apexcharts/apexcharts.min.js') }}"></script>

        <script src="{{ asset('themes/ubold/libs/selectize/js/standalone/selectize.min.js') }}"></script>

        <!-- Dashboar 1 init js-->
        <script src="{{ asset('themes/ubold/js/pages/dashboard-1.init.js') }}"></script>

        <!-- App js-->
        <script src="{{ asset('themes/ubold/js/app.min.js') }}"></script>
        
        <!-- Additional Javascript -->
        @yield('javascript')
    </body>
</html>