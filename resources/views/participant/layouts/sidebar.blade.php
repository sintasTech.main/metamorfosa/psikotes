<div class="right-bar">
    <div class="header">
        <h6 class="font-weight-medium text-center mt-2 text-uppercase">Group Chats</h6>
    </div>
    <div class="px-2">
        <div class="row">
            <div class="col-3 my-2">
                <div class="avatar bg-soft-secondary border-secondary border text-secondary font-18 avatar-title">
                    <!-- <i class="fe-heart font-22 avatar-title text-primary"></i> -->
                    1
                </div>
            </div>
            <div class="col-3 my-2">2</div>
            <div class="col-3 my-2">3</div>
            <div class="col-3 my-2">4</div>
            <div class="col-3 my-2">5</div>
            <div class="col-3 my-2">6</div>
        </div>
    </div>
  <!-- <div data-simplebar class="h-100"> -->

      <!-- Nav tabs -->
      <!-- <ul class="nav nav-tabs nav-bordered nav-justified" role="tablist">
          <li class="nav-item">
              <a class="nav-link py-2" data-toggle="tab" href="#chat-tab" role="tab">
                  <i class="mdi mdi-message-text d-block font-22 my-1"></i>
              </a>
          </li>
          <li class="nav-item">
              <a class="nav-link py-2" data-toggle="tab" href="#tasks-tab" role="tab">
                  <i class="mdi mdi-format-list-checkbox d-block font-22 my-1"></i>
              </a>
          </li>
          <li class="nav-item">
              <a class="nav-link py-2 active" data-toggle="tab" href="#settings-tab" role="tab">
                  <i class="mdi mdi-cog-outline d-block font-22 my-1"></i>
              </a>
          </li>
      </ul> -->

      <!-- Tab panes -->
      <!-- <div class="tab-content pt-0">

          
      </div> -->

  <!-- </div> end slimscroll-menu -->
</div>