<footer class="main-footer d-flex p-2 px-3 bg-white border-top">
    {{-- <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="#">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">About</a>
        </li>
    </ul> --}}

    <span class="copyright ml-auto my-auto mr-2">
        Made with
        <img src="http://static.skaip.org/img/emoticons/180x180/f6fcff/heart.gif" alt="♡" height="13px" style="color: #E02542;">
        <a href="//gitlab.com/sintasTech" rel="nofollow" style="text-decoration: none;">sintasTech</a>
    </span>
</footer>