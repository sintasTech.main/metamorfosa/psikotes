<div class="container-fluid mt-4">
    <div class="card-box welcome-card d-flex align-items-center">
        <div class="card-text">
            <h3 class="text-light m-0"><strong>Hallo {{ auth()->user()->person->full_name }}</strong></h3>
            <h3 class="text-light">Selamat datang di Sistem Ujian Psikotes Online <strong>Metamorfosa.</h3>
        </div>
    </div>
</div>