<footer class="footer">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6">
        <span class="copyright ml-auto my-auto mr-2">
            Made with
            <img src="http://static.skaip.org/img/emoticons/180x180/f6fcff/heart.gif" alt="♡" height="13px" style="color: #E02542;">
            <a href="//gitlab.com/sintasTech" rel="nofollow" style="text-decoration: none;">sintasTech</a>
        </span> 
      </div>
      <div class="col-md-6">
          <div class="text-md-right footer-links d-none d-sm-block">
              <a href="javascript:void(0);">About Us</a>
              <a href="javascript:void(0);">Help</a>
              <a href="javascript:void(0);">Contact Us</a>
          </div>
      </div>
    </div>
  </div>
</footer>