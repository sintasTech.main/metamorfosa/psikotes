@extends('participant.layouts.template')

@section('content')
@if ($message = Session::get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Overview</span>
            <h3 class="page-title">User Profile</h3>
        </div>
    </div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col-lg-4">
            <div class="card card-small mb-4 pt-3">
                <div class="card-header border-bottom text-center">
                    <div class="mb-3 mx-auto">
                        <img class="rounded-circle" src="{{ asset($user->person->avatar ? $user->person->avatar : 'assets/administrator/images/avatars/default.png') }}" alt="User Avatar" width="110"> </div>
                    <h4 class="mb-3">{{ $user->person->full_name }}</h4>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-2 pl-4 pr-4">
                        <div class="row">
                            <div class="col w-50">
                                <label class="m-0">Username</label>
                                <span class="d-block text-muted font-weight-bold">{{ $user->username }}</span>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item p-3 pb-2">
                        {{
                            Form::model($user, [
                                "method" => "PUT",
                                "route" => [$routeUserPrefix . ".profile.update-avatar"],
                                "files" => true
                            ])
                        }}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="form-group col-md-12 mb-0">
                                            <label>Update Avatar</label>
                                            <input type="file" name="avatar" class="form-control col-sm-12 @error('avatar') is-invalid @enderror">
                                        </div>
                                    </div>

                                    @error('avatar')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="material-icons">save</i>
                                    Update Avatar
                                </button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </li>
                </ul>

            </div>
        </div>

        <div class="col-lg-8">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-small mb-4">
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Update Password</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item p-3">
                                <div class="row">
                                    <div class="col">
                                        {{
                                            Form::model($user, [
                                                "route" => [$routeUserPrefix . ".profile.update-password"],
                                                "method" => "PUT"
                                            ])
                                        }}
                                        <div class="form-row">
                                            <div class="col-sm-12">
                                                <label>Current Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" placeholder="Current Password" autocomplete="new-password" required>

                                                    @error('current_password')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-6">
                                                <label>New Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" placeholder="New Password" autocomplete="new-password" required>

                                                    @error('new_password')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>New Confirm Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="new_confirm_password" class="form-control" placeholder="Confirm" autocomplete="new-password" required>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="material-icons">save</i>
                                            Update Password
                                        </button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection