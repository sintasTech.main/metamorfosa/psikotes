@extends('participant.layouts.template')

@section('content')
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">
        
        <!-- start page title -->
        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <!-- <h4 class="page-title">{{ $type->name }}</h4> -->
                    <!-- <p>{{ $type->description }}</p> -->
                </div>
            </div>
        </div>     
        <!-- end page title --> 

        <div class="row">
            <div class="col-md-3">
                <div class="card-box rounded-corner d-flex justify-content-between align-items-center">
                    <!-- <div class="d-flex align-items-center"></div> -->
                    <div class="avatar-sm bg-primary rounded-circle">
                        <i class="fe-clock avatar-title font-22 text-white"></i>
                    </div>
                    <div class="text-right">
                        <p class="text-muted mb-1 text-truncate">Sisa Waktu</p>
                        <h3 class="my-1"><span id="countdown">--:--:--</span></h3>
                    </div>
                </div>
                <div class="card-box">
                    <h4 class="header-title mb-3">Navigasi Soal</h4>
                    <div class="question-navigation">
                        @for ($i = 0; $i <= 8; $i++)
                        <button class="question-navigation__item page-link {{ $i == 2 ? 'active' : null }} {{ $i == 0 ? 'current' : null }}">{{ $i + 1 }}</button>
                        @endfor
                    </div>
                    <!-- <div class="pagination pagination-rounded">
                        <div class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">2</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">3</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">4</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">5</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">6</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">7</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">8</a></div>
                        <div class="page-item"><a class="page-link" href="javascript: void(0);">9</a></div>
                    </div> -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                            <p>Hi Coderthemes!</p>
                            <p>Clicking ‘Order Service’ on the right-hand side of the above page will present you with an order page. This service has the following Briefing Guidelines that will need to be filled before placing your order:</p>
                            <ol>
                                <li>Your design preferences (Color, style, shapes, Fonts, others) </li>
                                <li>Tell me, why is your item different? </li>
                                <li>Do you want to bring up a specific feature of your item? If yes, please tell me </li>
                                <li>Do you have any preference or specific thing you would like to change or improve on your item page? </li>
                                <li>Do you want to include your item's or your provider's logo on the page? if yes, please send it to me in vector format (Ai or EPS) </li>
                                <li>Please provide me with the copy or text to display</li>
                            </ol>

                            <p>Filling in this form with the above information will ensure that they will be able to start work quickly.</p>
                            <p>You can complete your order by putting your coupon code into the Promotional code box and clicking ‘Apply Coupon’.</p>
                            <p><b>Best,</b> <br> Graphic Studio</p>
                            
                            <div class="text-right mt-5">
                                <!-- <button class="btn btn-secondary mr-2"><i class="mdi mdi-reply mr-1"></i> Reply</button> -->
                                <button class="btn btn-primary">Selanjutnya <i class="mdi mdi-chevron-right-circle-outline ml-1"></i></button>
                            </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md px-4"> -->
                <!-- <div class="widget-rounded-circle card-box d-flex justify-content-between">
                    <div class="d-flex">
                        <div class="avatar-md rounded bg-secondary mr-2">
                            <i class="mdi mdi-book-multiple font-22 avatar-title"></i>
                        </div>
                        <div class="text-left d-flex flex-column justify-content-center">
                            <h4 class="mt-0">Sub Kategori 1</h4>
                            <p class="text-muted mb-0 text-truncate">120 Soal</p>
                        </div>
                    </div>
                    <div class="d-flex align-items-center">
                        <span class="badge badge-warning">Belum Selesai</span>
                    </div>
                </div> -->

                <!-- <div class="d-flex">
                    <p class="mr-2">1.</p>
                    <p>Lorem ipsum dolor sit amet, voluptatum cupiditate veritatis molestiae quas laudantium, saepe soluta aperiam fuga et officiis ipsum illum doloremque repellat atque ut quae architecto amet voluptatibus. Nulla tempora at aliquid ratione dolorum, voluptatum nihil id. Reprehenderit earum velit excepturi autem tempora vitae adipisci quos fuga nulla. Eius ratione, reiciendis nemo sit, quisquam magnam perferendis ullam vero distinctio, odio debitis. Cupiditate sequi iure ex repellendus totam quaerat ullam in mollitia dolorem quae sint ipsum, nam nulla porro inventore, illo ut! Corporis dolorum velit ad placeat nam reiciendis eaque dicta, sunt porro. Rerum!</p>
                </div>
                <div class="row pl-3">
                    <div class="col-12">
                        <div class="d-flex align-items-center mb-3">
                            <input class="mr-2" type="radio" name="answer" id="answer1">
                            <label class="m-0" for="answer1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut, eaque.</label>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <input class="mr-2" type="radio" name="answer" id="answer2">
                            <label class="m-0" for="answer2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut, eaque.</label>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <input class="mr-2" type="radio" name="answer" id="answer3">
                            <label class="m-0" for="answer3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut, eaque.</label>
                        </div>
                        <div class="d-flex align-items-center mb-3">
                            <input class="mr-2" type="radio" name="answer" id="answer4">
                            <label class="m-0" for="answer4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut, eaque.</label>
                        </div>
                    </div>
                </div> -->
            <!-- </div> -->
        </div>
        <!-- end row-->
        
    </div> <!-- container -->

</div> <!-- content -->
@endsection

@section('javascript')
<script>
const now = new Date();
const countDownDate = now.setMinutes(now.getMinutes() + 1);

const countdown = setInterval(() => {

  var now = new Date().getTime();
  const distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  const days = Math.floor(distance / (1000 * 60 * 60 * 24));
  const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((distance % (1000 * 60)) / 1000);

  document.getElementById("countdown").innerHTML = `${hours}:${minutes}:${seconds}`;

  if (distance < 0) {
    clearInterval(countdown);
    document.getElementById("countdown").innerHTML = "EXPIRED";
  }
}, 1000);
</script>
@endsection