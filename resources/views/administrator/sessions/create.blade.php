@extends('administrator.layouts.template')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.sessions.index') }}">Sesi Ujian</a></li>
                        <li class="breadcrumb-item active">Tambah</li>
                    </ol>
                </div>

                <h4 class="page-title">Tambah Sesi Ujian</h4>
            </div>
        </div>
    </div>

    @if ($errors->all())
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-alert-circle mr-2"></i>
                Something went wrong!
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('administrator.sessions.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Nama</strong>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" value="{{ old('name') }}">

                                    @error("name")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Deskripsi</strong>
                                <div class="form-group">
                                    <textarea type="description" rows="5" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Deskripsi sesi ujian...">{{ old('description') }}</textarea>

                                    @error("description")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Tipe Ujian</strong>
                                <div class="form-group">
                                    @forelse ($types as $typeId => $typeName)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="type[{{ $typeId }}]" name="type[{{ $typeId }}]" value="{{ $typeId }}">
                                        <label class="custom-control-label" for="type[{{ $typeId }}]">{{ $typeName }}</label>
                                    </div>
                                    @empty
                                    <div class="col-sm-12 text-center">
                                        <span class="font-italic text-danger">Maaf, kamu harus menambahkan tipe ujian terlebih dahulu.</span>
                                    </div>
                                    @endforelse

                                    @if ($errors->has('type'))
                                    <input type="hidden" class="form-control is-invalid">
                                    <div class="invalid-feedback">Something went wrong.</div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form__action">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                <i class="mdi mdi-content-save mr-1s"></i>Simpan
                            </button>
                            <a class="btn btn-secondary waves-effect waves-light text-white mt-1 mb-1" href="{{ route('administrator.sessions.index') }}">
                                <i class="mdi mdi-reply mr-1s"></i>Batal
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection