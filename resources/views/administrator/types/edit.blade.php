@extends('administrator.layouts.template')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.types.index') }}">Tipe Ujian</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>
                <h4 class="page-title">Edit Tipe Ujian</h4>
            </div>
        </div>
    </div>

    @if ($message = session()->get("success"))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-check-all mr-2"></i>
                {{ $message }}
            </div>
        </div>
    </div>
    @endif

    @if ($errors->all())
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-alert-circle mr-2"></i>
                Something went wrong!
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="form-wrapper">
                    <form method="POST" action="{{ route('administrator.types.update', $type->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Nama</strong>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" value="{{ old('name', $type->name) }}">

                                    @error("name")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Deskripsi</strong>
                                <div class="form-group">
                                    <textarea type="description" rows="5" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Deskripsi tipe ujian...">{{ old('description', $type->description) }}</textarea>

                                    @error("description")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form__action">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                <i class="mdi mdi-content-save mr-1"></i>Simpan
                            </button>
                            <a class="btn btn-secondary waves-effect waves-light text-white mt-1 mb-1" href="{{ route('administrator.types.index') }}">
                                <i class="mdi mdi-reply mr-1"></i>Batal
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection