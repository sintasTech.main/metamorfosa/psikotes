@extends('administrator.layouts.template')

@section('content')
<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Dashboard</span>
            <h3 class="page-title">Overview</h3>
        </div>
    </div>
    <!-- End Page Header -->

    <!-- Content -->
    <div class="row">
        @if ($authUserRole === $settingRole["superadmin"])
        <div class="col-lg col-md-6 col-sm-6 mb-4">
            <div class="stats-small stats-small--1 card card-small">
                <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                            <span class="stats-small__label text-uppercase">Permissions</span>
                            <h6 class="stats-small__value count my-3">{{ $permissions->count() }}</h6>
                        </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-1"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg col-md-6 col-sm-6 mb-4">
            <div class="stats-small stats-small--1 card card-small">
                <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                            <span class="stats-small__label text-uppercase">Roles</span>
                            <h6 class="stats-small__value count my-3">{{ $roles->count() }}</h6>
                        </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-2"></canvas>
                </div>
            </div>
        </div>
        @endif

        @if (in_array($authUserRole, $settingRole))
        <div class="col-lg col-md-4 col-sm-6 mb-4">
            <div class="stats-small stats-small--1 card card-small">
                <div class="card-body p-0 d-flex">
                    <div class="d-flex flex-column m-auto">
                        <div class="stats-small__data text-center">
                            <span class="stats-small__label text-uppercase">Users</span>
                            <h6 class="stats-small__value count my-3">{{ $users->count() }}</h6>
                            <small>
                                There are
                                <span class="text-success font-weight-bold">
                                    {{ $users->where("is_active", 1)->count() }}
                                    active
                                </span>
                                and
                                <span class="text-danger font-weight-bold">
                                    {{ $users->where("is_active", 0)->count() }}
                                    inactive
                                </span>
                                users.
                            </small>
                        </div>
                    </div>
                    <canvas height="120" class="blog-overview-stats-small-3"></canvas>
                </div>
            </div>
        </div>
        @endif
    </div>
    <!-- Content -->
</div>
@endsection