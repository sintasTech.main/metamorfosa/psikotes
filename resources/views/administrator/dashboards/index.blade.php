@extends('administrator.layouts.template')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-xl-3">
            <div class="card-box bg-pattern">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-md bg-blue rounded">
                            <i class="fe-voicemail avatar-title font-22 text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark my-1"><span data-plugin="counterup">{{ $sessionCount }}</span></h3>
                            <p class="text-muted mb-0 text-truncate">Sessions</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->

        <div class="col-lg-6 col-xl-3">
            <div class="card-box bg-pattern">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-md bg-success rounded">
                            <i class="fe-users avatar-title font-22 text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark my-1"><span data-plugin="counterup">{{ $participantCount }}</span></h3>
                            <p class="text-muted mb-0 text-truncate">Peserta</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->
        <div class="col-lg-6 col-xl-3">
            <div class="card-box bg-pattern">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-md bg-danger rounded">
                            <i class="fe-book avatar-title font-22 text-white"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark my-1"><span data-plugin="counterup">{{ $typeCount }}</span></h3>
                            <p class="text-muted mb-0 text-truncate">Type Test</p>
                        </div>
                    </div>
                </div>
            </div> <!-- end card-box-->
        </div> <!-- end col -->
        <div class="col-lg-6 col-xl-3">
            <div class="card-box bg-pattern">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-md bg-warning rounded">
                            <!-- <i class="fe-dollar-sign avatar-title font-22 text-white"></i> -->
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <!-- <h3 class="text-dark my-1">$<span data-plugin="counterup">256</span>k</h3>
                            <p class="text-muted mb-0 text-truncate">Booked Revenue</p> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="card-box">
                <h4 class="header-title mb-3">Sessions Terdekat</h4>
                <div class="table-responsive">
                    <table class="table table-borderless table-nowrap table-hover table-centered m-0">
                        <thead class="thead-light">
                            <tr>
                                <th>Nama</th>
                                <th style="width: 10%">Peserta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <h5 class="m-0 font-weight-normal">PWNU DIY</h5>
                                </td>

                                <td>
                                    20
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <h5 class="m-0 font-weight-normal">Universitas Jogja</h5>
                                </td>

                                <td>
                                    12
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <h5 class="m-0 font-weight-normal">SMK 69</h5>
                                </td>

                                <td>
                                    10
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card-box">
                <div class="" style="min-height: 230px"></div>
            </div>
        </div>
    </div>
</div>
@endsection