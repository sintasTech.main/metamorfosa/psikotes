@extends('administrator.layouts.template')

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
        <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
            <span class="text-uppercase page-subtitle">Overview</span>
            <h3 class="page-title">User Profile</h3>
        </div>
    </div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col-lg-4">
            <div class="card card-small mb-4 pt-3">
                <div class="card-header border-bottom text-center">
                    <div class="mb-3 mx-auto">
                        <img class="rounded-circle" src="{{ asset($user->person->avatar ? $user->person->avatar : 'assets/administrator/images/avatars/default.png') }}" alt="User Avatar" width="110"> </div>
                    <h4 class="mb-0">{{ $user->person->full_name }}</h4>
                    <span class="text-muted d-block mb-2">{{ $user->roles->first()->name }}</span>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-2 pl-4 pr-4">
                        <strong class="text-muted d-block mt-2 mb-2">{{ $user->username }}</strong>
                    </li>

                    <li class="list-group-item p-3 pb-2">
                        {{
                            Form::model($user, [
                                "method" => "PUT",
                                "route" => [$routeUserPrefix . ".profile.update-avatar"],
                                "files" => true
                            ])
                        }}
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="form-group col-md-12 mb-0">
                                            <label>Update Avatar</label>
                                            <input type="file" name="avatar" class="form-control col-sm-12 @error('avatar') is-invalid @enderror">
                                        </div>
                                    </div>

                                    @error('avatar')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-sm btn-primary">
                                    <i class="material-icons">save</i>
                                    Update Avatar
                                </button>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </li>
                </ul>

            </div>
        </div>

        <div class="col-lg-8">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Account Details</h6>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                        <div class="row">
                            <div class="col">
                                {{
                                    Form::model($user, [
                                        "route" => [$routeUserPrefix . ".profile.update"],
                                        "method" => "PUT"
                                    ])
                                }}
                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Name</label>
                                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name', $user->person->full_name) }}" required>

                                        @error('name')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label>Username</label>
                                        <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" placeholder="Username" value="{{ old('username', $user->username) }}" required>

                                        @error('username')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                @if (in_array($authUserRole, $settingRole))
                                <div class="form-row">
                                    <div class="form-group col-sm-12">
                                        <label class="d-block mb-2">Role</label>
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                            @foreach ($roles as $item_id => $item)
                                            <label class="btn btn-white {{ $item_id === $userRole ? "active" : "" }}">
                                                <input type="radio" name="role" value="{{ $item_id }}" {{ $item_id === $userRole ? "checked" : "" }}>
                                                {{ $item }}
                                            </label>
                                            @endforeach
                                        </div>

                                        <div class="form-group m-0">
                                            <input class="form-control @error('role') is-invalid @enderror" hidden>
                                            @error("role")
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                @endif

                                <button type="submit" class="float-left btn btn-primary">
                                    <i class="material-icons">save</i>
                                    Update
                                </button>
                                <button type="button" class="float-right btn btn-danger" onclick="handleDelete()">
                                    <i class="material-icons">delete</i>
                                    Delete
                                </button>
                                {{ Form::close() }}


                                {{
                                    Form::open([
                                        "method" => "DELETE",
                                        "route" => [$routeUserPrefix . ".profile.destroy"],
                                        "id" => "form-delete"
                                    ])
                                }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-small mb-4">
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Update Password</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item p-3">
                                <div class="row">
                                    <div class="col">
                                        {{
                                            Form::model($user, [
                                                "route" => [$routeUserPrefix . ".profile.update-password"],
                                                "method" => "PUT"
                                            ])
                                        }}
                                        <div class="form-row">
                                            <div class="col-sm-12">
                                                <label>Current Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" placeholder="Current Password" autocomplete="new-password" required>

                                                    @error('current_password')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-sm-6">
                                                <label>New Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" placeholder="New Password" autocomplete="new-password" required>

                                                    @error('new_password')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>New Confirm Password</label>
                                                <div class="form-group">
                                                    <input type="password" name="new_confirm_password" class="form-control" placeholder="Confirm" autocomplete="new-password" required>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">
                                            <i class="material-icons">save</i>
                                            Update Password
                                        </button>
                                        {{ Form::close() }}
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    const handleDelete = () => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById(`form-delete`).submit();
                }
            });
    }
</script>
@endsection