@extends('administrator.layouts.template')

@section('content')

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">   
                                    </div>
                                    <h4 class="page-title">Jawaban</h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        <div class="col text-left">
                                        <div class="col text-right">
                                            <a href="javascript: void(0);" class="btn btn-xs btn-light" style="padding: 5px 10px;"><i class="mdi mdi-plus-circle"></i></a>
                                        </div>
                                        
                            <div class="col-xl-6">
                                <div class="card-box">
                                    <div class="dropdown float-right">
                                    </div>
                                        
                                    <div class="table-responsive">
                                        <table class="table table-borderless table-nowrap table-hover table-centered m-0">

                                            <thead class="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Jawaban</th>
                                                    <th>No Telp</th>
                                                    <th>Password</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">Themes Market</h5>
                                                    </td>
    
                                                    <td>
                                                        Oct 15, 2018
                                                    </td>
    
                                                    <td>
                                                        $5848.68
                                                    </td>
    
                                                    <td>
                                                        <span class="badge bg-soft-warning text-warning">Upcoming</span>
                                                    </td>
    
                                                    <td>
                                                        <a href="javascript: void(0);" class="btn btn-xs btn-light"><i class="mdi mdi-pencil"></i></a>
                                                        <a href="javascript: void(0);" class="btn btn-xs btn-light"><i class="mdi mdi-delete"></i></a>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <h5 class="m-0 font-weight-normal">Freelance</h5>
                                                    </td>
    
                                                    <td>
                                                        Oct 12, 2018
                                                    </td>
    
                                                    <td>
                                                        $1247.25
                                                    </td>
    
                                                    <td>
                                                        <span class="badge bg-soft-success text-success">Paid</span>
                                                    </td>
    
                                                    <td>
                                                        <a href="javascript: void(0);" class="btn btn-xs btn-light"><i class="mdi mdi-pencil"></i></a>
                                                        <a href="javascript: void(0);" class="btn btn-xs btn-light"><i class="mdi mdi-delete"></i></a>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div> <!-- end .table-responsive-->
                                </div> <!-- end card-box-->
                            </div> <!-- end col -->
                        </div>
                        <!-- end row -->
                        
                   
@endsection