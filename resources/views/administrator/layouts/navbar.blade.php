<div class="navbar-custom">
    <div class="container-fluid">
        <ul class="list-unstyled topnav-menu float-right mb-0">
            <li class="dropdown d-none d-lg-inline-block">
                <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light" data-toggle="fullscreen" href="#">
                    <i class="fe-maximize noti-icon"></i>
                </a>
            </li>

            <li class="dropdown notification-list topbar-dropdown">
                <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <img src="{{ asset(auth()->user()->person->avatar ? auth()->user()->person->avatar : 'assets/administrator/images/avatars/default.png') }}" alt="user-image" class="rounded-circle">
                    <span class="pro-user-name ml-1">
                        {{ auth()->user()->person->full_name }}
                        <i class="mdi mdi-chevron-down"></i>
                    </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-user"></i>
                        <span>Profil Saya</span>
                    </a>

                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item notify-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout').submit();">
                        <i class="fe-log-out"></i>
                        <span>Logout</span>
                        <form id="logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>
                </div>
            </li>
        </ul>

        <div class="logo-box">
            <a href="{{ route('administrator.dashboards.index') }}" class="logo logo-light text-center">
                <span class="logo-sm">
                    <img src="{{ asset('themes/ubold/images/metamorfosa.png') }}" alt="" height="20">
                    <!-- <span class="logo-lg-text-light">Metamorfosa</span> -->
                </span>
                <span class="logo-lg">
                    <img src="{{ asset('themes/ubold/images/metamorfosa.png') }}" alt="" height="40">
                    <!-- <span class="logo-lg-text-light">Metamorfosa</span> -->
                </span>
            </a>
        </div>

        <ul class="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
                <button class="button-menu-mobile waves-effect waves-light">
                    <i class="fe-menu"></i>
                </button>
            </li>
        </ul>

        <div class="clearfix"></div>
    </div>
</div>