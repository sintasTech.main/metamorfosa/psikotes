<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js h-100">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <?php $route = explode(".", Illuminate\Support\Facades\Route::currentRouteName()); ?>
    <title>{{ ucwords($route[1]) }} &mdash; {{ config("app.name", "Laravel") }}</title>
    <meta name="description" content="Made with love by sintasTech">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('themes/ubold/images/metamorfosa.ico') }}">

    <!-- App css -->
    <link href="{{ asset('themes/ubold/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
    <link href="{{ asset('themes/ubold/css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

    <link href="{{ asset('themes/ubold/css/bootstrap-dark.min.css') }}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
    <link href="{{ asset('themes/ubold/css/app-dark.min.css') }}" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />

    <!-- icons -->
    <link href="{{ asset('themes/ubold/css/icons.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- additional css -->
    @yield('css')
</head>

<body data-layout='{"mode": "light", "width": "fluid", "menuPosition": "fixed", "sidebar": { "color": "light", "size": "default", "showuser": false}, "topbar": {"color": "dark"}, "showRightSidebarOnPageLoad": true}'>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- Topbar Start -->
        @include('administrator.layouts.navbar')
        <!-- Topbar End-->

        <!-- Left Sidebar Start -->
        @include('administrator.layouts.sidebar')
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <div class="content">
                @yield('content')
            </div>

            <!-- Footer Start -->
            @include('administrator.layouts.footer')
            <!-- end Footer -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->
    </div>
    <!-- End page -->

    <!-- Vendor js -->
    <script src="{{ asset('themes/ubold/js/vendor.min.js') }}"></script>

    <!-- App js-->
    <script src="{{ asset('themes/ubold/js/app.min.js') }}"></script>

    <!-- Additional Javascript -->
    @yield('javascript')
</body>

</html>