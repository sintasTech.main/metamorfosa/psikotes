<div class="left-side-menu">
    <div class="h-100" data-simplebar>
        <div id="sidebar-menu">
            <ul id="side-menu">
                <li class="{{
                    request()->routeIs($routeUserPrefix . '.dashboards.*')
                    ? 'menuitem-active' : ''
                }}">
                    <a href="{{ route($routeUserPrefix. '.dashboards') }}">
                        <i data-feather="airplay"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="{{
                    request()->routeIs($routeUserPrefix . '.types.*')
                    || request()->routeIs($routeUserPrefix . '.categories.*')
                    || request()->routeIs($routeUserPrefix . '.questions.*')
                    ? 'menuitem-active' : ''
                }}">
                    <a href="{{ route($routeUserPrefix. '.types.index') }}">
                        <i data-feather="grid"></i>
                        <span>Tipe</span>
                    </a>
                </li>

                <li class="{{
                    request()->routeIs($routeUserPrefix . '.sessions.*')
                    ? 'menuitem-active' : ''
                }}">
                    <a href="{{ route($routeUserPrefix. '.sessions.index') }}">
                        <i data-feather="clock"></i>
                        <span>Sesi</span>
                    </a>
                </li>

                <li class="{{
                    request()->routeIs($routeUserPrefix . '.users*')
                    ? 'menuitem-active' : ''
                }}">
                    <a href="#users" data-toggle="collapse">
                        <i data-feather="user"></i>
                        <span>Pengguna</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div id="users" class="collapse {{
                            request()->routeIs($routeUserPrefix . '.users*')
                            ? 'show' : ''
                        }}">
                        <ul class="nav-second-level">
                            <li class="{{
                                request()->routeIs($routeUserPrefix . '.users.admin*')
                                ? 'menuitem-active' : ''
                            }}">
                                <a href="{{ route($routeUserPrefix. '.users.admin.index') }}">Admin</a>
                            </li>
                            <li class="{{
                                request()->routeIs($routeUserPrefix . '.users.participant*')
                                ? 'menuitem-active' : ''
                            }}">
                                <a href="{{ route($routeUserPrefix. '.users.participant.index') }}">Peserta</a>
                            </li>
                            <li>
                                <a href="{{ route($routeUserPrefix. '.users.index') }}">Semua</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>

        <div class="clearfix"></div>
    </div>
</div>