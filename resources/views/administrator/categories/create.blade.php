@extends('administrator.layouts.template')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.types.index') }}">Tipe Ujian</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('administrator.types.show', $type->id) }}">Kategori Tipe Ujian {{ $type->name }}</a></li>
                        <li class="breadcrumb-item active">Tambah</li>
                    </ol>
                </div>

                <h4 class="page-title">Tambah Kategori Tipe Ujian {{ $type->name }}</h4>
            </div>
        </div>
    </div>

    @if ($errors->all())
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-alert-circle mr-2"></i>
                Something went wrong!
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="form-wrapper">
                    <form method="POST" action="{{ route('administrator.categories.store', $type->id) }}">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <strong class="text-muted d-block mb-2">Nama</strong>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" value="{{ old('name') }}">

                                    @error("name")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-6">
                                <strong class="text-muted d-block mb-2">Waktu <span class="small-text">(Menit)</span></strong>
                                <div class="form-group">
                                    <input type="text" name="time" class="form-control @error('time') is-invalid @enderror" placeholder="Waktu" value="{{ old('time') }}">

                                    @error("time")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Deskripsi</strong>
                                <div class="form-group">
                                    <textarea type="description" rows="5" name="description" class="form-control @error('description') is-invalid @enderror" placeholder="Deskripsi">{{ old('description') }}</textarea>

                                    @error("description")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Petunjuk Pengerjaan</strong>
                                <div class="form-group">
                                    <textarea rows="5" name="rule" class="form-control @error('rule') is-invalid @enderror" placeholder="Petunjuk Pengerjaan">{{ old('rule') }}</textarea>

                                    @error("rule")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Contoh</strong>
                                <div class="form-group">
                                    <textarea rows="5" name="example" class="form-control @error('example') is-invalid @enderror" placeholder="Contoh">{{ old('example') }}</textarea>

                                    @error("example")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form__action">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                <i class="mdi mdi-content-save mr-1"></i>Simpan
                            </button>
                            <a class="btn btn-secondary waves-effect waves-light text-white mt-1 mb-1" href="{{ route('administrator.types.show', $type->id) }}">
                                <i class="mdi mdi-reply mr-1"></i>Batal
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection