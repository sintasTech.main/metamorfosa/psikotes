@extends('administrator.layouts.template')

@section('css')
<link href="{{ asset('themes/ubold/libs/mohithg-switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.types.index') }}">Tipe Ujian</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('administrator.types.show', $category->type_id) }}">Kategori Tipe Ujian {{ $category->type->name }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('administrator.categories.show', $category->id) }}">Soal Kategori {{ $category->name }}</a></li>
                        <li class="breadcrumb-item active">Semua</li>
                    </ol>
                </div>

                <h4 class="page-title">Lihat Kategori Tipe Ujian {{ $category->type->name }}</h4>
            </div>
        </div>
    </div>

    @if ($message = session()->get("success"))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-check-all mr-2"></i>
                {{ $message }}
            </div>
        </div>
    </div>
    @endif

    @if ($errors->all())
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-alert-circle mr-2"></i>
                Something went wrong!
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box pb-1">
                <div class="header">
                    <h4 class="header-title">{{ $category->name ?? '-' }}</h4>
                    <p>{{ $category->description ?? '-' }}</p>
                </div>
                <div class="header">
                    <h4 class="header-title">Petunjuk</h4>
                    <p>{{ $category->rule ?? '-' }}</p>
                </div>
                <div class="header">
                    <h4 class="header-title">Contoh</h4>
                    <p>{{ $category->example ?? '-' }}</p>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="float col-sm-12 p-0 mb-2">
                        <div class="float-left text-left mb-3">
                            <h4>Soal-Soal Kategori {{ $category->name }}</h4>
                        </div>
                        <div class="text-right">
                            <a href="{{ route('administrator.questions.create', $category->id) }}" class="btn btn-sm btn-primary waves-effect waves-light">
                                <i class="mdi mdi-plus-circle mr-1"></i>
                                Tambah Soal
                            </a>
                        </div>
                    </div>

                    <div class="table-responsive mt-3">
                        <table class="table table-borderless table-centered">
                            <thead class="thead-light">
                                <tr>
                                    <th style="width: 8%;">No</th>
                                    <th>Deskripsi</th>
                                    <th class="text-center" style="width: 10%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($category->questions as $key => $question)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>Soal Nomor {{ $key + 1 }}</td>
                                    <td class="text-center d-flex justify-content-center">
                                        <a href="{{ route('administrator.questions.edit', $question->id) }}" class="btn btn-sm btn-info waves-effect waves-light mr-1">
                                            <i class="mdi mdi-pencil"></i>
                                        </a>
                                        {{
                                            Form::open([
                                                "method" => "DELETE",
                                                "route" => ["administrator.questions.destroy", $question->id],
                                                "id" => "form-delete[".$question->id."]" 
                                            ])
                                        }}
                                        <button class="btn btn-sm btn-danger waves-effect waves-light" type="button" onclick="handleDelete(<?= $question->id ?>)">
                                            <i class="mdi mdi-delete"></i>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="3" class="text-center font-italic">Data not found</td>
                                </tr>
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr class="active">
                                    <td colspan="2">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Footable -->
<script src="{{ asset('themes/ubold/libs/footable/footable.all.min.js') }}"></script>

<!-- Switchery -->
<script src="{{ asset('themes/ubold/libs/mohithg-switchery/switchery.min.js') }}"></script>

<script>
    const handleDelete = (id) => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById(`form-delete[${ id }]`).submit();
                }
            });
    }

    $(".table").footable();
    $(".demo-show-entries").change(function(o) {
        o.preventDefault();
        var t = $(this).val();
        $(this).data("page-size", t), $(this).trigger("footable_initialized")
    });

    $('[data-plugin="switchery"]').each(function(e, a) {
        new Switchery($(this)[0], $(this).data());
    });
</script>
@endsection