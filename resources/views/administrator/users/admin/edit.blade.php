@extends('administrator.layouts.template')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.users.index') }}">Users</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('administrator.users.admin.index') }}">Admin</a></li>
                        <li class="breadcrumb-item active">Edit</li>
                    </ol>
                </div>

                <h4 class="page-title">Edit Pengguna Admin</h4>
            </div>
        </div>
    </div>

    @if ($message = session()->get("success"))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-check-all mr-2"></i>
                {{ $message }}
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mb-3">Detail Akun</h4>

                    {{
                        Form::model($user, [
                            "method" => "PUT",
                            "route" => ["administrator.users.admin.update", $user->id]
                        ])
                    }}

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Nama" value="{{ old('name', $user->person->full_name) }}" required>

                                @error("name")
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" placeholder="Username" value="{{ old('username', $user->username) }}" required>

                                @error("username")
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email', $user->email) }}" required>

                                @error("email")
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                <i class="mdi mdi-content-save"></i>
                                Simpan
                            </button>
                            <a class="btn btn-secondary waves-effect waves-light text-white mt-1 mb-1 mr-2" href="{{ route('administrator.users.admin.index') }}">
                                <i class="mdi mdi-reply"></i>
                                Batal
                            </a>
                            <button type="button" class="btn btn-danger waves-effect waves-light text-white mt-1 mb-1" onclick="handleDelete()">
                                <i class="mdi mdi-delete"></i>
                                Hapus
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}

                    {{
                        Form::open([
                            "method" => "DELETE",
                            "route" => ["administrator.users.destroy", $user->id],
                            "id" => "form-delete"
                        ])
                    }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    {{
                        Form::model($user, [
                            "method" => "PUT",
                            "route" => ["administrator.users.update-avatar", $user->id],
                            "files" => true
                        ])
                    }}
                    <div class="row mb-3">
                        <div class="mx-auto">
                            <img class="rounded-circle img-thumbnail" src="{{ asset($user->person->avatar ? $user->person->avatar : '/assets/administrator/images/avatars/default.png') }}" alt="Avatar" width="110">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="file" name="avatar" class="form-control-sm form-control-file p-0 @error('avatar') is-invalid @enderror" required>

                                @error("avatar")
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                <i class="mdi mdi-content-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mb-3">Kata Sandi</h4>

                    {{
                        Form::model($user, [
                            "method" => "PUT",
                            "route" => ["administrator.users.update-password", $user->id],
                            "class" => $errors->has('current_password', 'new_password') ? 'was-validated' : ""
                        ])
                    }}

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Kata Sandi Sekarang</label>
                                <input type="password" name="current_password" class="form-control @error('name') is-invalid @enderror" placeholder="Kata Sandi Sekarang" autocomplete="current-password" required>

                                @error("current_password")
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Kata Sandi Baru</label>
                                <input type="password" name="new_password" class="form-control @error('new_password') is-invalid @enderror" placeholder="Kata Sandi Baru" autocomplete="new-password" required>

                                @error("new_password")
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Konfirmasi Kata Sandi Baru</label>
                                <input type="password" name="new_confirm_password" class="form-control" placeholder="Konfirmasi Kata Sandi Baru" autocomplete="confirm-password" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                <i class="mdi mdi-content-save"></i>
                                Simpan
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    const handleDelete = () => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById(`form-delete`).submit();
                }
            });
    }
</script>
@endsection