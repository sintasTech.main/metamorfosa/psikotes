@extends('administrator.layouts.template')

@section('css')
<link href="{{ asset('themes/ubold/') }}/libs/mohithg-switchery/switchery.min.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.users.index') }}">Users</a></li>
                        <li class="breadcrumb-item active">Semua</li>
                    </ol>
                </div>

                <h4 class="page-title">Semua Pengguna</h4>
            </div>
        </div>
    </div>

    @if ($message = session()->get("success"))
    <div class="row">
        <div class="col-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-check-all mr-2"></i>
                {{ $message }}
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-sm-12 p-0 mb-2">
                        <div class="text-right">
                            <a href="{{ route('administrator.users.admin.create') }}" class="btn btn-sm btn-primary waves-effect waves-light mb-2 mr-1">
                                <i class="mdi mdi-plus-circle mr-1"></i>
                                Tambah Admin
                            </a>

                            <a href="{{ route('administrator.users.participant.create') }}" class="btn btn-sm btn-primary waves-effect waves-light mb-2 mr-1">
                                <i class="mdi mdi-plus-circle mr-1"></i>
                                Tambah Peserta
                            </a>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table mb-0 table-hover table-borderless table-centered toggle-arrow-tiny" data-page-size="10">
                            <thead class="thead-light text-center">
                                <tr>
                                    <th data-toggle="true" style="width: 7%;">#</th>
                                    <th>Nama</th>
                                    <th data-hide="all">Username</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                    <th style="width: 10%;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($users as $item)
                                <tr>
                                    <td class="text-center text-nowrap">{{ ++$i }}</td>
                                    <td>{{ $item->person->full_name }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->roles->first()->name }}</td>
                                    <td class="text-center">
                                        <div class="switchery-demo">
                                            <input type="checkbox" class="toggle-status" data-id="{{ $item->id }}" data-plugin="switchery" data-color="#64b0f2" data-size="small" {{ $item->is_active ? "checked" : "" }} />
                                        </div>
                                    </td>
                                    <td class="d-flex justify-content-center">
                                        <a href="{{ route('administrator.users.' . strtolower($item->roles->first()->name) . '.edit', $item->id) }}" class="btn btn-sm btn-info waves-effect waves-light mr-1">
                                            <i class="mdi mdi-pencil"></i>
                                        </a>

                                        {{
                                            Form::open([
                                                "method" => "DELETE",
                                                "route" => ['administrator.users.destroy', $item->id],
                                                "id" => "form-delete[".$item->id."]"
                                            ])
                                        }}
                                        <button type="button" class="btn btn-sm btn-danger waves-effect waves-light" onclick="handleDelete(<?= $item->id ?>)">
                                            <i class="mdi mdi-delete"></i>
                                        </button>
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                                @empty
                                @endforelse
                            </tbody>
                            <tfoot>
                                <tr class="active">
                                    <td colspan="5">
                                        <div class="text-right">
                                            <ul class="pagination pagination-split justify-content-end footable-pagination m-t-10"></ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<!-- Sweetalert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Footable -->
<script src="{{ asset('themes/ubold/libs/footable/footable.all.min.js') }}"></script>

<!-- Switchery -->
<script src="{{ asset('themes/ubold/libs/mohithg-switchery/switchery.min.js') }}"></script>

<script>
    const handleDelete = (id) => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById(`form-delete[${ id }]`).submit();
                }
            });
    }

    $(function() {
        $(".toggle-status").change(function() {
            const csrfToken = $('meta[name="csrf-token"]').attr("content");
            const status = $(this).prop("checked") === true ? 1 : 0;
            const user_id = $(this).data("id");

            $.ajax({
                type: "POST",
                url: `<?= route('administrator.users.index') ?>/${user_id}/update-status`,
                dataType: "json",
                data: {
                    "_method": "PUT",
                    "_token": csrfToken,
                    "status": status
                },
                success: function(response) {
                    console.log(response)
                }
            });
        });

        $(".table").footable();
        $(".demo-show-entries").change(function(o) {
            o.preventDefault();
            var t = $(this).val();
            $(this).data("page-size", t), $(this).trigger("footable_initialized")
        });

        $('[data-plugin="switchery"]').each(function(e, a) {
            new Switchery($(this)[0], $(this).data());
        });
    });
</script>
@endsection