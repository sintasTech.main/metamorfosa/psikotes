@extends('administrator.layouts.template')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.users.index') }}">Users</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('administrator.users.participant.index') }}">Peserta</a></li>
                        <li class="breadcrumb-item active">Tambah</li>
                    </ol>
                </div>

                <h4 class="page-title">Tambah Pengguna Peserta</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('administrator.users.participant.store') }}" class="needs-validation @if(session()->has('errors')) was-validated @endif">
                        @csrf

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Sesi</label>
                                    {!! Form::select('session', $sessions, old('session'), [
                                    'class' => 'form-control' . ($errors->has('session') ? ' is-invalid' : '' ),
                                    'placeholder' => 'Pilih salah satu...',
                                    'required' => 'required'
                                    ]) !!}

                                    @error("session")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Jumlah Peserta</label>
                                    <input type="number" name="participant_count" class="form-control @error('participant_count') is-invalid @enderror" placeholder="Jumlah Peserta" value="{{ old('participant_count', 1) }}" required>

                                    @error("participant_count")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                    <i class="mdi mdi-content-save"></i>
                                    Simpan
                                </button>
                                <a class="btn btn-secondary waves-effect waves-light text-white mt-1 mb-1" href="{{ route('administrator.users.participant.index') }}">
                                    <i class="mdi mdi-reply"></i>
                                    Batal
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection