@extends('administrator.layouts.template')

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($errors->all())
<div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-info mx-2"></i>
    <strong>Something went wrong!</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Permission</h6>
                </div>
                <ul class="list-group list-group-flush">
                    {{
                        Form::model($permission, [
                            "route" => ["administrator.permissions.update", $permission->id],
                            "method" => "PUT"
                        ])
                    }}
                    <li class="list-group-item p-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <strong class="text-muted d-block mb-2">Name</strong>
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name', $permission->name) }}" required>

                                    @error("name")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="list-group-item pt-3 pb-3 pl-4 pr-4">
                        <div class="clearfix flex-row">
                            <div class="float-left">
                                <button type="submit" class="btn btn-sm btn-primary mt-1 mb-1 mr-2" style="width: 80px;">
                                    <i class="material-icons">save</i>
                                    Save
                                </button>

                                <a class="btn btn-sm btn-secondary text-white mt-1 mb-1 mr-2" href="{{ route("administrator.permissions.index") }}" style="width: 80px;">
                                    <i class="material-icons">reply</i>
                                    Cancel
                                </a>
                            </div>

                            <div class="float-right">
                                <button class="btn btn-sm btn-danger mt-1 mb-1" type="button" onclick="handleDelete()" style="width: 80px;">
                                    <i class="material-icons">delete</i>
                                    Delete
                                </button>
                            </div>
                        </div>
                    </li>
                    {{ Form::close() }}


                    {{
                        Form::open([
                            "method" => "DELETE",
                            "route" => ["administrator.permissions.destroy", $permission->id],
                            "id" => "form-delete"
                        ])
                    }}
                    {{ Form::close() }}
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    const handleDelete = () => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById(`form-delete`).submit();
                }
            });
    }
</script>
@endsection