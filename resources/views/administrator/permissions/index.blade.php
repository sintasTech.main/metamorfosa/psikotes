@extends('administrator.layouts.template')

@section('content')
@if ($message = session()->get("success"))
<div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
    <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close" style="outline: none;">
        <span aria-hidden="true">×</span>
    </button>
    <i class="fa fa-check mx-2"></i>
    <strong>{{ $message }}</strong>
</div>
@endif

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters mb-4"></div>
    <!-- End Page Header -->

    <div class="row">
        <div class="col">
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="float-left m-0 mt-1">Permissions</h6>
                    <div class="float-right">
                        <a href="{{ route('administrator.permissions.create') }}" class="btn btn-sm btn-success">
                            <i class="material-icons">add</i>
                            Add
                        </a>
                    </div>
                </div>
                <div class="card-body p-0 text-center table-responsive">
                    <table class="table table-hover">
                        <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">#</th>
                                <th scope="col" class="border-0">Name</th>
                                <th scope="col" class="border-0">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($permissions as $item)
                            <tr>
                                <td class="align-middle" style="width: 50px;">{{ ++$i }}</td>
                                <td class="text-left align-middle">{{ $item->name }}</td>
                                <td class="align-middle" style="width: 250px; min-width: 180px;">
                                    <div class="row m-0">
                                        <div class="col mr-1 ml-1 p-0">
                                            <a href="{{ route('administrator.permissions.edit', $item->id) }}">
                                                <button type="button" class="btn btn-sm btn-info col-sm-12">
                                                    <i class="material-icons">create</i>
                                                    Edit
                                                </button>
                                            </a>
                                        </div>
                                        <div class="col mr-1 ml-1 p-0">
                                            {{
                                                Form::open([
                                                    "method" => "DELETE",
                                                    "route" => ["administrator.permissions.destroy", $item->id],
                                                    "id" => "form-delete[".$item->id."]"
                                                ])
                                            }}
                                            <button class="btn btn-sm btn-danger col-sm-12" type="button" onclick="handleDelete(<?= $item->id ?>)">
                                                <i class="material-icons">delete</i>
                                                Delete
                                            </button>
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="3" class="text-center font-italic">Data not found</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="pagination justify-content-center">
                    {{ $permissions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script>
    const handleDelete = (id) => {
        swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this item!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    document.getElementById(`form-delete[${ id }]`).submit();
                }
            });
    }
</script>
@endsection