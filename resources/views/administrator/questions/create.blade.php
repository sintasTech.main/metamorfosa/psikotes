@extends('administrator.layouts.template')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="{{ route('administrator.types.index') }}">Tipe Ujian</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('administrator.types.show', $category->type->id) }}">Kategori Tipe Ujian {{ $category->type->name }}</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('administrator.categories.show', $category->id) }}">Soal Kategori {{ $category->name }}</a></li>
                        <li class="breadcrumb-item active">Tambah</li>
                    </ol>
                </div>

                <h4 class="page-title">Tambah Soal Kategori {{ $category->name }}</h4>
            </div>
        </div>
    </div>

    @if ($errors->all())
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <i class="mdi mdi-alert-circle mr-2"></i>
                Something went wrong!
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="form-wrapper">
                    <form method="POST" action="{{ route('administrator.questions.store', $category->id) }}">
                        @csrf

                        <div class="row">
                            <div class="col-md-12">
                                <strong class="text-muted d-block mb-2">Deskripsi</strong>
                                <div class="form-group">
                                    <textarea id="description" name="description" class="form-control" placeholder="Deskripsi">{{ old('description') }}</textarea>

                                    @error("description")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <strong class="text-muted d-block mb-2">Tampilan Jawaban</strong>
                                <div class="form-group">
                                    {!! Form::select('answer_type', [
                                    'vertical' => "VERTIKAL",
                                    'horizontal' => "HORIZONTAL"
                                    ], old('type'), [
                                    'class' => 'form-control' . ($errors->has('answer_type') ? ' is-invalid' : '' ),
                                    'required' => ''
                                    ]) !!}

                                    @error("answer_type")
                                    <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form__action">
                            <button type="submit" class="btn btn-success waves-effect waves-light mt-1 mb-1 mr-2">
                                <i class="mdi mdi-content-save mr-1"></i>Simpan
                            </button>
                            <a class="btn btn-secondary waves-effect waves-light text-white mt-1 mb-1" href="{{ route('administrator.categories.show', $category->id) }}">
                                <i class="mdi mdi-reply mr-1"></i>Batal
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('javascript')
<script src="https://cdn.tiny.cloud/1/pckcmughhg5fui9hxvf5hqnmy0e0t9iw8mwmhl23tp204d4j/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: "#description",
        height: 400,
        setup: function(editor) {
            editor.on('init change', function() {
                editor.save();
            });
        },
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste imagetools"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i'
        ],
        image_title: true,
        automatic_uploads: true,
        images_upload_url: "{{ route('administrator.questions.upload-image', ['_token' => csrf_token()]) }}",
        file_picker_types: "image",
        relative_urls: false,
        remove_script_host: true,
        document_base_url: "/",
        convert_urls: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement("input");
            input.setAttribute("type", "file");
            input.setAttribute("accept", "image/*");
            input.onchange = function() {
                var file = this.files[0];

                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function() {
                    var id = "blobid" + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(",")[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), {
                        title: file.name
                    });
                };
            };
            input.click();
        }
    });
</script>
@endsection