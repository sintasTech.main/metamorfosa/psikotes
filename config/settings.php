<?php

return [
    "role" => [
        "superadmin" => env("ROLE_SUPERADMIN"),
        "admin" => env("ROLE_ADMIN")
    ]
];