<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionTableSeeder::class,
            UserTableSeeder::class,
            SessionTableSeeder::class,
            TypeTableSeeder::class,
            SessionTypeTableSeeder::class,
            CategoryTableSeeder::class,
            QuestionAndAnswerTableSeeder::class,
            SessionUserTableSeeder::class
        ]);
    }
}
