<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\User;
use App\Models\Person;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $superadminUser = User::create([
            "username" => "superadmin",
            "email" => "superadmin@sintastech.com",
            "password" => bcrypt("yogyakarta"),
            "is_active" => 1
        ]);
        $role = Role::create(["name" => "Super Admin"]);
        $permissions = Permission::pluck("id", "id")->all();
        $role->syncPermissions($permissions);
        $superadminUser->assignRole([$role->id]);

        $adminUser = User::create([
            "username" => "admin",
            "email" => "admin@sintastech.com",
            "password" => bcrypt("indonesia"),
            "is_active" => 1
        ]);
        $role = Role::create(["name" => "Admin"]);
        $adminUser->assignRole([$role->id]);

        $participantUser = User::create([
            "username" => "participant",
            "email" => "participant@sintastech.com",
            "password" => bcrypt("indonesia"),
            "is_active" => 1
        ]);
        $role = Role::create(["name" => "Participant"]);
        $participantUser->assignRole([$role->id]);

        Person::insert([
            [
                "user_id" => $superadminUser->id,
                "full_name" => "sintasTech",
                "avatar" => "seeds/images/avatars/default.png"
            ],
            [
                "user_id" => $adminUser->id,
                "full_name" => "Admin User",
                "avatar" => "seeds/images/avatars/default.png"
            ],
            [
                "user_id" => $participantUser->id,
                "full_name" => "Participant User",
                "avatar" => "seeds/images/avatars/default.png"
            ]
        ]);
    }
}
