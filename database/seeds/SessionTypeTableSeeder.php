<?php

use Illuminate\Database\Seeder;

use App\Models\SessionType;

class SessionTypeTableSeeder extends Seeder
{
    public function run()
    {
        $sessiontypes = [
            [
                "session_id" => 1,
                "type_id" => 1
            ],
            [
                "session_id" => 1,
                "type_id" => 2
            ],
            [
                "session_id" => 2,
                "type_id" => 1
            ],
            [
                "session_id" => 3,
                "type_id" => 2
            ]
        ];

        foreach ($sessiontypes as $sessiontype) {
            SessionType::create([
                "session_id" => $sessiontype["session_id"],
                "type_id" => $sessiontype["type_id"],
            ]);
        }
    }
}
