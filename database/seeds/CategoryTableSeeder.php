<?php

use Illuminate\Database\Seeder;

use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        $categories = [
            [
                "type_id" => 1,
                "name" => "AAAA",
                "time" => 90
            ],
            [
                "type_id" => 2,
                "name" => "BBBB",
                "time" => 60
            ],
            [
                "type_id" => 2,
                "name" => "CCCC",
            ]
        ];

        foreach ($categories as $category) {
            Category::create($category);
        }
    }
}
