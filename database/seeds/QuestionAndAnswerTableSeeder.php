<?php

use Illuminate\Database\Seeder;

use App\Models\Question;
use App\Models\Answer;
use App\Models\Option;

class QuestionAndAnswerTableSeeder extends Seeder
{
    public function run()
    {
        $question1 = Question::create([
            "category_id" => 1,
            "description" => "Berapa hasil 1 + 2?",
        ]);
        $question2 = Question::create([
            "category_id" => 1,
            "description" => "Berapa hasil 5 x 5 ?",
        ]);
        $question3 = Question::create([
            "category_id" => 1,
            "description" => "Berapa hasil 13 + 37?",
        ]);

        foreach ([
            [
                "name" => "a. 1",
                "weight" => 0,
            ],
            [
                "name" => "b. 2",
                "weight" => 0,
            ],
            [
                "name" => "c. 3",
                "weight" => 1,
            ],
            [
                "name" => "d. 4",
                "weight" => 0,
            ]
        ] as $option) {
            Option::create([
                "question_id" => $question1->id,
                "description" => $option["name"],
                "weight" => $option["weight"]
            ]);
        }

        foreach ([
            [
                "name" => "a. 20",
                "weight" => 0,
            ],
            [
                "name" => "b. 25",
                "weight" => 1,
            ],
            [
                "name" => "c. 30",
                "weight" => 0,
            ],
            [
                "name" => "d. 35",
                "weight" => 0,
            ]
        ] as $option) {
            Option::create([
                "question_id" => $question2->id,
                "description" => $option["name"],
                "weight" => $option["weight"]
            ]);
        }

        foreach ([
            [
                "name" => "a. 47",
                "weight" => 0,
            ],
            [
                "name" => "b. 48",
                "weight" => 0,
            ],
            [
                "name" => "c. 49",
                "weight" => 0,
            ],
            [
                "name" => "d. 50",
                "weight" => 1,
            ]
        ] as $option) {
            Option::create([
                "question_id" => $question3->id,
                "description" => $option["name"],
                "weight" => $option["weight"]
            ]);
        }

        foreach ([
            [
                "user_id" => 3,
                "question_id" => 1,
                "option_id" => 3
            ],
            [
                "user_id" => 3,
                "question_id" => 2,
                "option_id" => 5
            ],
            [
                "user_id" => 3,
                "question_id" => 3,
                "option_id" => 11
            ]
        ] as $answer) {
            Answer::create($answer);
        }
    }
}
