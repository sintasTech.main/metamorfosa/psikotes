<?php

use Illuminate\Database\Seeder;
use App\Models\Session;

class SessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sessions = [
            [
                "name" => "Ujian Psikotes PT. Batu Bara Ghifary",
                "description" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            ],
            [
                "name" => "Psikotes SMA Negeri 1 Bantul",
                "description" => "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."
            ],
            [
                "name" => "Psikotes SMA Negeri 3 Semarang",
                "description" => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."
            ]
        ];

        foreach ($sessions as $session) {
            Session::create($session);
        }
    }
}
