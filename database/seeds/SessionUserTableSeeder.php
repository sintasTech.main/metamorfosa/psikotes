<?php

use Illuminate\Database\Seeder;

use App\Models\SessionUser;

class SessionUserTableSeeder extends Seeder
{
    public function run()
    {
        $sessionUsers = [
            [
                "session_id" => 1,
                "user_id" => 3
            ]
        ];

        foreach ($sessionUsers as $sessionUser) {
            SessionUser::create([
                "session_id" => $sessionUser["session_id"],
                "user_id" => $sessionUser["user_id"]
            ]);
        }
    }
}
