<?php

use Illuminate\Database\Seeder;

use App\Models\Type;

class TypeTableSeeder extends Seeder
{
    public function run()
    {
        $types = [
            [
                "name" => "IST",
            ],
            [
                "name" => "CFIT",
                "description" => "Skala 3 Bentuk A"
            ]
        ];

        foreach ($types as $type) {
            Type::create($type);
        }
    }
}
