<?php

use Illuminate\Support\Facades\Route;

$this->settingRole = config("settings.role");

// Route::group([
//     "namespace" => "Home",
//     "as" => "home.",
// ], function () {
//     Route::get("/", ["uses" => "DashboardsController@index"])->name("index");
// });

Route::namespace("Auth")->group(function () {
    Route::get("/", ["uses" => "LoginController@showLoginForm"])->name("home.index");
    Route::get("login", 'LoginController@showLoginForm')->name("login");
    Route::post("login", 'LoginController@login')->name("login");
    Route::post("logout", 'LoginController@logout')->name("logout");
});

Route::middleware(["auth"])->group(function () {
    //Administrator
    Route::group([
        "middleware" => ["role:" . implode("|", $this->settingRole)],
        "namespace" => "Administrator",
        "as" => "administrator.",
        "prefix" => "administrator"
    ], function () {
        Route::get("/", "DashboardsController@index")->name("dashboards");
        Route::resource("dashboards", "DashboardsController")->only(["index"]);

        Route::group([
            "as" => "profile.",
            "prefix" => "profile"
        ], function () {
            Route::get("/", "ProfileController@index")->name("index");
            Route::put("/update", "ProfileController@update")->name("update");
            Route::delete("/destroy", "ProfileController@updatePassword")->name("destroy");
            Route::put("/update-password", "ProfileController@updatePassword")->name("update-password");
            Route::put("/update-avatar", "ProfileController@updateAvatar")->name("update-avatar");
        });

        Route::group([
            "middleware" => ["role:" . $this->settingRole["superadmin"]],
        ], function () {
            Route::resource("roles", "RolesController")->except(["show"]);
            Route::resource("permissions", "PermissionsController")->except(["show"]);
        });

        Route::resource("users", "UsersController")->only(["index", "destroy"]);
        Route::group([
            "as" => "users.",
            "prefix" => "users"
        ], function () {
            Route::put("/{id}/update-password", "UsersController@updatePassword")->name("update-password");
            Route::put("/{id}/update-status", "UsersController@updateStatus")->name("update-status");
            Route::put("/{id}/update-avatar", "UsersController@updateAvatar")->name("update-avatar");

            Route::get("/admin", "UsersController@adminIndex")->name("admin.index");
            Route::get("/admin/create", "UsersController@adminCreate")->name("admin.create");
            Route::post("/admin/create", "UsersController@adminStore")->name("admin.store");
            Route::get("/admin/{id}/edit", "UsersController@adminEdit")->name("admin.edit");
            Route::put("/admin/{id}/edit", "UsersController@adminUpdate")->name("admin.update");

            Route::get("/participant", "UsersController@participantIndex")->name("participant.index");
            Route::get("/participant/create", "UsersController@participantCreate")->name("participant.create");
            Route::post("/participant/create", "UsersController@participantStore")->name("participant.store");
            Route::get("/participant/{id}/edit", "UsersController@participantEdit")->name("participant.edit");
            Route::put("/participant/{id}/edit", "UsersController@participantUpdate")->name("participant.update");
        });

        Route::resource("sessions", "SessionsController");
        Route::resource("types", "TypesController");

        Route::resource("categories", "CategoriesController")->only(["show", "edit", "update", "destroy"]);
        Route::get("categories/{typeId}/create", "CategoriesController@create")->name("categories.create");
        Route::post("types/{typeId}", "CategoriesController@store")->name("categories.store");

        Route::resource("questions", "QuestionsController")->only(["edit", "update", "destroy"]);
        Route::get("questions/{categoryId}/create", "QuestionsController@create")->name("questions.create");
        Route::post("categories/{categoryId}", "QuestionsController@store")->name("questions.store");
        Route::post("questions/upload-image", "QuestionsController@uploadImage")->name("questions.upload-image");

        Route::resource("answers", "AnswersController")->except(["show"]);
    });

    //Participant
    Route::group([
        "middleware" => ["role:Participant"],
        "namespace" => "Participant",
        "as" => "participant.",
        "prefix" => "participant"
    ], function () {
        Route::get("/", "DashboardsController@index")->name("dashboards");
        Route::resource("types", "TypesController");

        Route::group([
            "as" => "profile.",
            "prefix" => "profile"
        ], function () {
            Route::get("/", "ProfileController@index")->name("index");
            Route::put("/update-password", "ProfileController@updatePassword")->name("update-password");
            Route::put("/update-avatar", "ProfileController@updateAvatar")->name("update-avatar");
        });
    });
});
